game.input.mouse = {
	//Secondary variables
		pressed: [],
		moved: [],
		released: [],
		
		mode: 0,
	
	//Methods
		catch: function(x, y, action) {
			switch (this.mode) {
				case 0:
					({
						press: this.pressed,
						movement: this.moved,
						release: this.released
					})[action].push([x, y]);
					
					break;
			}
		},
		
		flush: function() {
			this.pressed.length = 0;
			this.moved.length = 0;
			this.released.length = 0;
		}
};
